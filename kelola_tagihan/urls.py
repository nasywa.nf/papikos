from django.urls import path, re_path
from .views import *

urlpatterns = [
    path('', daftar_tagihan, name='daftar_tagihan'),
    re_path(r'^(?P<tagihan_id>\d+)/$', detail_tagihan, name='detail_tagihan'),
    re_path(r'^(?P<tagihan_id>\d+)/pembayaran/$', bayar_tagihan, name='bayar_tagihan'),
    re_path(r'^(?P<tagihan_id>\d+)/pembayaran/tunai$', pembayaran_tunai, name='pembayaran_tunai'),
    re_path(r'^(?P<tagihan_id>\d+)/pembayaran/transfer$', pembayaran_transfer, name='pembayaran_transfer'),
    path('kelola-tagihan/', kelola_tagihan, name='kelola_tagihan'),
    path('buat-tagihan/', buat_tagihan, name='buat_tagihan'),
    path('detail-tagihan-pemilik/', detail_tagihan_pemilik, name='detail_tagihan_pemilik')
]

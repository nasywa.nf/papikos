from django.apps import AppConfig


class KelolaTagihanConfig(AppConfig):
    name = 'kelola_tagihan'

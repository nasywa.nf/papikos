from django.shortcuts import render

# Create your views here.
def daftar_tagihan(request):
    # Temporary dummy card
    card = {
        'title' : "Rp2.500.000",
        'date' : "15 Agustus 2020",
        'time' : "11:45",
        'status' : "Belum Dibayar",
    }

    card2 = {
        'title' : "Rp2.500.000",
        'date' : "15 Agustus 2020",
        'time' : "11:45",
        'status' : "Menunggu Konfirmasi",
    }

    card3 = {
        'title' : "Rp2.500.000",
        'date' : "15 Agustus 2020",
        'time' : "11:45",
        'status' : "Menunggu Konfirmasi",
    }
    
    context = {
        'card_data' : card, 
        'card_data2' : card2,
        'card_data3' : card3,
    }
    return render(request, 'kelola_tagihan/daftar_tagihan.html', context)

def detail_tagihan(request, tagihan_id):
    biaya_tambahan = [
        {
            'detail' : "Service AC",
            'biaya' : "Rp 300.000",
        },
        {
            'detail' : "Tagihan Listrik",
            'biaya' : "Rp 200.000",
        }
    ]

    tagihan = {
        'total_biaya' : "Rp2.500.000",
        'date' : "15 Agustus 2020",
        'time' : "11:45",
        'status' : "Belum Dibayar",
        'biaya_sewa' : "Rp 2.000.000",
        'biaya_tambahan' : biaya_tambahan,
    }

    context = {
        'id' : tagihan_id,
        'tagihan' : tagihan,
        'id_next' : int(tagihan_id)+1,
    }
    return render(request, 'kelola_tagihan/detail_tagihan.html', context)

def bayar_tagihan(request, tagihan_id):
    context = {
        'id' : tagihan_id,
    }
    return render(request, 'kelola_tagihan/bayar_tagihan.html', context)

def detail_tagihan_pemilik(request):
    return render(request, 'kelola_tagihan/detail_tagihan_pemilik.html')

def pembayaran_tunai(request, tagihan_id):
    context = {
        'id' : tagihan_id,
    }
    return render(request, 'kelola_tagihan/pembayaran_tunai.html', context)

def pembayaran_transfer(request, tagihan_id):
    context = {
        'id' : tagihan_id,
    }
    return render(request, 'kelola_tagihan/pembayaran_transfer.html', context)
    
def kelola_tagihan(request):
    card_data = {
        'title' : "Rp2.500.000",
        'date' : "15 Agustus 2020",
        'time' : "11:45",
        'status' : "Belum Dibayar",
        'person' : "Astrida Nayla",
        'erasable' : True
    }
    context = {
        'data' : card_data, 
    }
    return render(request, 'kelola_tagihan/kelola_tagihan.html', context)

def buat_tagihan(request):
    return render(request, 'kelola_tagihan/buat_tagihan.html')


from django.shortcuts import render

# Create your views here.
def home_view(request):
    # Temporary dummy card
    card = {
        'title' : "Judul Card",
        'person' : "Astrida Nayla F",
        'desc' : "tulisan apa gitu ceritanya",
        'desc_titipan' : "Halo Nak, ini ada paket sembako baru datang kemarin siang, saya taruh dekat tangga ya.",
        'date' : "15 Agustus 2020",
        'time' : "11:45",
        'status' : "Belum Dibayar",
        'editable': True,
    }
    context = {
        'card_data': card,
    }
    return render(request, 'homepage/home.html', context)
from django.urls import path
from . import views
from .views import *

app_name = 'homepage'

urlpatterns = [
    path('home/', home_view, name='home')
]
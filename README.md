[![pipeline status](https://gitlab.com/nasywa.nf/papikos/badges/master/pipeline.svg)](https://gitlab.com/nasywa.nf/papikos/-/commits/master) 

# Papikos
#### Kelompok 11 | RPL-C

### Anggota:
    1. Astrida Nayla Fauzia (1806235826)
    2. Mahdia Aliyya Nuha Kiswanto (1806141290)
    3. Ali Irsyaad Nursya'ban (1806141132)
    4. Nasywa Nur Fathiyah (1806205546)

### Tentang Project
PAPIKOS (Platform Penghubung Pemilik dan Penyewa Kos) merupakan sistem manajemen dan administrasi kos yang memudahkan pemilik dan penyewa kos untuk berinteraksi dan bertransaksi secara cepat dan efisien. 

Sistem ini merupakan aplikasi yang terhubung secara daring sehingga aktivitas pada sistem dapat dilakukan dengan lebih fleksibel. Aplikasi ini merupakan suatu platform yang menjembatani kebutuhan pemilik kos kepada penyewa kos dan sebaliknya.  

### Project Link
http://papikos11.herokuapp.com/
from django.shortcuts import render
# Create your views here.


def view_pengaduan(request):
    return render(request, 'view_pengaduan.html')

def daftar_pengaduan_pemilik(request):
    card1 = {
        'title' : "Air Kamar Mandi Tidak Nyala",
        'desc' : "Kamar Mandi Lantai 3 sebelah kiri airnya mati Bu..",
        'date' : "1 Januari 2020",
        'time' : "11:45",
        'status' : "Submitted",
        'person' : "Astrida Nayla",
    }

    card2 = {
        'title' : "Lampu Kamar Mati",
        'desc' : "Lampu kamar 301 mati bu",
        'date' : "1 Januari 2020",
        'time' : "11:45",
        'status' : "Submitted",
        'person' : "Ali Irsyaad",
    }

    card3 = {
        'title' : "Air AC Bocor",
        'desc' : "Air AC selalu menetes tiap malam.",
        'date' : "15 Agustus 2019",
        'time' : "11:45",
        'status' : "Diproses",
        'person' : "Astrida Nayla",
    }

    card4 = {
        'title' : "Ada Kecoa",
        'desc' : "Kecoa terbang di kamar mandi. hiii.",
        'date' : "15 Agustus 2019",
        'time' : "11:45",
        'status' : "Selesai",
        'person' : "Astrida Nayla",
    }

    context = {
        'card_data1' : card1, 
        'card_data2' : card2,
        'card_data3' : card3,
        'card_data4' : card4, 
    }
    return render(request, 'daftar_pengaduan.html', context)

def detail_pengaduan_pemilik(request, pengaduan_id):
    pengaduan = {
        'title' : "Ada Kecoa",
        'desc' : "Kecoa terbang di kamar mandi. hiii.",
        'date' : "15 Agustus 2019",
        'time' : "11:45",
        'status' : "Selesai",
        'person' : "Astrida Nayla",
    }
    
    context = {
        'pengaduan' : pengaduan,
    }
    return render(request, 'detail_pengaduan.html', context)

def buat_pengaduan(request):
    return render(request, 'buat_pengaduan.html')


def edit_pengaduan(request):
    return render(request, 'edit_pengaduan.html')

def ubah_status(request):
    return render(request, 'ubah_status.html')
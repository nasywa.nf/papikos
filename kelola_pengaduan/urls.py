from django.urls import path, re_path
from .views import *

appname = 'kelola_pengaduan'

urlpatterns = [
    path('', view_pengaduan, name='view_pengaduan'),
    path('buat/', buat_pengaduan, name='buat_pengaduan'),
    path('edit/', edit_pengaduan, name='edit_pengaduan'),
    path('pemilik/', daftar_pengaduan_pemilik, name='daftar_pengaduan_pemilik'),
    re_path(r'pemilik/(?P<pengaduan_id>\d+)/$', detail_pengaduan_pemilik, name='detail_pengaduan_pemilik'),
    path('ubah-status/', ubah_status, name='ubah_status'),
]

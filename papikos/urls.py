"""papikos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views

# static files
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls'), name='homepage'),
    path('pbt/', include('pegumuman_barang_titipan.urls'), name='pengumuman_barang_titipan'),
    path('tagihan/', include('kelola_tagihan.urls'), name="kelola_tagihan"),
    path('pengaduan/', include('kelola_pengaduan.urls'), name="pengaduan"),
    path('', include('registrasi.urls'), name="registrasi"),
]

urlpatterns += staticfiles_urlpatterns()

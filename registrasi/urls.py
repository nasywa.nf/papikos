from django.urls import path
from . import views
from .views import *

appname = 'registrasi'

urlpatterns = [
    path('role/', pilih_role_view, name='pilih_role'),
    path('', signin_view, name='signin'),
    path('sign-up/', signup_view, name='signup')
]
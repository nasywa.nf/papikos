from django import forms

class SigninForm(forms.Form):
    username = forms.CharField(label = 'Username', max_length = 50, required = True, widget=forms.TextInput(attrs={
		'placeholder' : 'Masukkan username'
	}))
    password = forms.CharField(label = 'Password', required = True, widget=forms.PasswordInput(attrs={
		'placeholder' : 'Masukkan password'
	}))

class SignupForm(forms.Form):
	nama = forms.CharField(label = 'Nama', max_length = 50, required = True, widget=forms.TextInput(attrs={
		'placeholder' : 'Masukkan nama'
	}))
	tanggal_lahir = forms.DateField(label = 'Tanggal Lahir', required = True, widget=forms.DateInput(attrs={
		'placeholder' : 'Hari/Bulan/Tahun',
        'type' : 'date'
	}))
	no_telpon = forms.CharField(label = 'No Telpon', required = True, widget=forms.TextInput(attrs={
		'placeholder' : 'Masukkan no telpon'
	}))
	email = forms.EmailField(label = 'Email', max_length = 50, required = True, widget=forms.EmailInput(attrs={
		'placeholder' : 'example@example.com'
	}))
	username = forms.CharField(label = 'Username', max_length = 50, required = True, widget=forms.TextInput(attrs={
		'placeholder' : 'Masukkan username'
	}))
	password = forms.CharField(label = 'Password', required = True, widget=forms.PasswordInput(attrs={
		'placeholder' : 'Masukkan password'
	}))
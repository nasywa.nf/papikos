from django.shortcuts import render
from .forms import SigninForm, SignupForm

# Create your views here.
def pilih_role_view(request):
    context = {}
    return render(request, 'registrasi/role.html', context)

def signin_view(request):
    form = SigninForm()
    context = {
        'form' : form
    }
    return render(request, 'registrasi/signin.html', context)

def signup_view(request):
    form = SignupForm()
    context = {
        'form' : form
    }
    return render(request, 'registrasi/signup.html', context)
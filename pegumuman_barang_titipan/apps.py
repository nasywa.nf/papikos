from django.apps import AppConfig


class PegumumanBarangTitipanConfig(AppConfig):
    name = 'pegumuman_barang_titipan'

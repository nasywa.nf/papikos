from django.urls import path
from . import views
from .views import *

appname = 'pengumuman_barang_titipan'

urlpatterns = [
    path('', pbt_view, name='pbt_view'),
    path('penyewa/', pbt_view_penyewa, name='pbt_view_penyewa'),
    path('edit/', pbt_edit, name='pbt_edit'),
    path('create/', pbt_create, name='pbt_create'),
]
from django.shortcuts import render
# Create your views here.
def pbt_view(request):
    return render(request, 'pbt_view.html')

def pbt_edit(request):
    return render(request, 'pbt_edit.html')

def pbt_create(request):
    return render(request, 'pbt_create.html')

def pbt_view_penyewa(request):
    return render(request, 'pbt_view_penyewa.html')